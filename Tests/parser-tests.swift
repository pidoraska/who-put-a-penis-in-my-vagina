import XCTest
@testable import Sigil

final class ParserTests: XCTestCase {
   
   func testParserCanHandleDataDeclGrammar() {
      let example1 =
      "data Disj {A B : U2} (Left : A, Right : B) : 'max (A) (B)'\n" +
      "| Inl (left : Left)\n" +
      "| Inr (right : Right)\n"
      XCTAssertNoThrow(try parse(text: example1))
      let example2 =
      "data Disj (Left : A, Right : B) : 'max (A) (B)'\n" +
      "| Inl (left : Left)\n" +
      "| Inr (right : Right)\n"
      XCTAssertNoThrow(try parse(text: example2))
      let example3 = "data M{L:J}(M:P):N|Dot|Dot|D"
      XCTAssertNoThrow(try parse(text: example3))
      let example4 =
      "data Disj : 'max (A) (B)'\n" +
      "| Inl (left : Left)\n" +
      "| Inr (right : Right)\n"
      XCTAssertNoThrow(try parse(text: example4))
      let example5 =
      "data Disj : 'max (A) (B)'\n"
      XCTAssertNoThrow(try parse(text: example5))
      let example6 = "data 'you know... Ther is a kind of thing to expect!!!' : XD"
      XCTAssertNoThrow(try parse(text: example6))
      let example7 = "data '()()()()' : '()'"
      XCTAssertThrowsError(try parse(text: example7))
      let example8 =
      "data N : U | Z | 'S #' (n : N)"
      XCTAssertNoThrow(try parse(text: example8))
   }
   func testParserRecognisesDefinitons() throws {
      _ = try {
         let t = "def a : b = c"
         _ = try parse(text: t)
      }()
   }
   
   func testParserCanHandleUnicode() {
      let a = "data Первое : Второе | Третье (четвёртое:Пятое)"
      XCTAssertNoThrow(try parse(text:a))
      let b = "data 太陽 : 海 | 遊艇 (海鷗:鳥)"
      XCTAssertNoThrow(try parse(text:b))
      let c = "data Первое : Второе | Третье (четвёртое:Пятое)"
      XCTAssertNoThrow(try parse(text:c))
      let d = "data '太陽 太陽 太陽' : 海 | 遊艇 (海鷗:鳥)"
      XCTAssertNoThrow(try parse(text:d))
      let e = "data '∆ℷ⊅⊋∠∐∏∑⨊⨕≍⊚⊺⨻⪨⧰𝐬𝚥𝑸𝓉' : '∁ℇ∄⊅∆∃⊇ℶ∞∓∩=¬×'"
      XCTAssertNoThrow(try parse(text:e))
   }
   
   func testParserCanHandleExpressions() {
      let a = "data K : 'universe from ('some multiverse ('much peculiar!')')'"
      XCTAssertNoThrow(try parse(text: a))
      let b = "data K : '(a) b ('c (d) ('ef')') g (h)'"
      XCTAssertNoThrow(try parse(text: b))
   }
   
   func testParserCanHandleFuncDecls() {
      let a =
      "fn 'project # through #' {A : U, 'B #' :  U}(i : A, t: 'B (i)') : M\n" +
      "| `idx, `smth => 'a ('d ('oo') e f') b (lmn) c'\n"
      XCTAssertNoThrow(try parse(text: a))
      let b =
      "fn 'project # through #' {A : U, 'B #' :  U}(i : A, t: 'B (i)') : M\n" +
      "  | `k = m, `m = m => 'a ('d ('oo') e f') b (lmn) c'\n"
      let te = try? parse(text: b)
      XCTAssert(te != nil)
      let c =
      "fn 'project # through #' {A : U, 'B #' :  U}(i : A, t: 'B (i)') : M\n" +
      " | _, a => ? a, b\n" +
      "  | j, _ => ? j\n" +
      "   | _ => 'you are gourgeous'\n" +
      "  | _, _ => ok\n" +
      "| r, m => rf\n"
      XCTAssertNoThrow(try parse(text: c))
      let d =
      "fn 'project # through #' {A : U, 'B #' :  U}(i : A, t: 'B (i)') : M\n" +
      "  | _, h => ? k, 'a (b) c'\n" +
      "   | j, _ => ? j \n" +
      "    | _ => 'you are gourgeous'\n"
      XCTAssertNoThrow(try parse(text: d))
      let e =
      "fn 'project # through #' {A : U, 'B #' : '(A) -> (U)'}(i : A, t: 'B (i)') : M\n" +
      "|\n_,\na\n=>\n?\na,\n'a (b) c'\n\n\n\n\n\n\n\n\n" +
      " |\nj,\n_\n=>\nm\n\n\n\n\n\n\n" +
      " |\n_\n=>\n'you are gourgeous'\n\n\n\n\n\n\n\n"
      XCTAssertNoThrow(try parse(text: e))
      let f =
      "fn M ( a : b ) : M | '( a  ) b ( c  )' => k\n"
      XCTAssertNoThrow(try parse(text: f))
      let g =
      "fn '# + #' (l : N, r : N) : N\n" +
      "| a, 'S ( b )' => '(  'S(a)'  ) + ( b )'\n" +
      "| a, Z => a\n"
      XCTAssertNoThrow(try parse(text: g))
      let h =
      "fn a (m : r) : b\n| `a => `a\n"
      XCTAssertNoThrow(try parse(text: h))
      let fail1 =
      "fn 'project # through #' {A : U, 'B #' :  U}(i : A, t: 'B (i)') : M\n" +
      "  | _, h => ? k, 'a (b) c'\n" +
      "  | j, _ => ? j \n" +
      "    | _ => 'you are gourgeous'\n"
      XCTAssertThrowsError(try parse(text: fail1))
      
   }
   
   func testParserCanHandleUniverseDecls() {
      let a = "univ 'Some nice thing that youll like', 'Even better one'"
      XCTAssertNoThrow(try parse(text: a))
      let b = "univ A, B, C\n" + "univ D, B\n"
      XCTAssertNoThrow(try parse(text: b))
   }
   
   func testParserCanHandleRuleDecls() {
      let a = "rule N : M, A : B, R : L, B : D, K : P, D : A"
      XCTAssertNoThrow(try parse(text: a))
      let b =
      "\n rule \n N \n : \n M \n , \n A \n : \n B \n , \n R \n : \n L \n , \n B \n : \n D"
      XCTAssertNoThrow(try parse(text: b))
   }
   
   func testPatternSymbolsAreContracted () {
      let ex = "fn a (c : d) : e | 'a + (b)' => k\n"
      let env = try! parse(text: ex)
      let fil: [Mapping] = env.compactMap({ if case let .mapping(mp) = $0 { return mp }; return nil })
      let fun = fil[0]
      let pat = fun.patterns[0]
      switch pat.stencil[0].repr {
         case let .compound(ch, _):
            XCTAssert(ch == "a+#".chars, "Symbol is not contracted but should have been")
         default: XCTAssert(false, "unreachable")
      }
   }
   
   func testDefsAreRecognised() {
      _ = {
         let t = "def 'something good' {A : B} : J = 'just (zero)'"
         let cond = try? parse(text: t)
         XCTAssert(cond != nil)
      }()
      _ = {
         let t = "def 'something good' {A : B} (a : A) : J = 'just (zero)'"
         let cond = try? parse(text: t)
         XCTAssert(cond == nil)
      }()
   }
}
