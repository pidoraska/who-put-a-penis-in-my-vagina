//
//  File.swift
//  
//
//  Created by Cromo Beingnur on 20/11/21.
//

import XCTest
@testable import Sigil

class SemaTests : XCTestCase {
   
   func testSemaDiagnosesInvalidRawEnvs () {
      _ = {
         let t1 =
         "univ A\n" +
         "data Dot : 'Something that doesnt exist' | point\n" +
         "fn 'just #' (a : Dot) : Dot\n" +
         "| _ => 'fake expr'\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .invalidReference)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ A\n" +
         "data Dot : A | point\n" +
         "fn 'just #' (a : Dot) : Dot\n" +
         "| _ => 'fake expr'\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .invalidReference)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "def a : U = b\n" +
         "def b : U = c"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .invalidReference)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
   }
   
   func testSemaCanDetectCycles() {
      let a = "rule N : M, A : B, R : L, B : D, K : P, D : A"
      XCTAssertThrowsError(try {
         let text = try parse(text: a)
         let rules: [RulePack] = text.compactMap({
            if case let .rules(rp) = $0 { return rp }
            else { return nil }
         })
         _ = try sanitiseRules(rules, text: a.chars)
      }())
      let a2 = "rule N : M, A : B, D : R, D : A, R : L, B : D, K : P"
      XCTAssertThrowsError(try {
         let text = try parse(text: a2)
         let rules: [RulePack] = text.compactMap({
            if case let .rules(rp) = $0 { return rp }
            else { return nil }
         })
         _ = try sanitiseRules(rules, text: a2.chars)
      }())
      let c = "rule A : B, B : C, C : A"
      XCTAssertThrowsError(try {
         let text = try parse(text: c)
         let rules: [RulePack] = text.compactMap({
            if case let .rules(rp) = $0 { return rp }
            else { return nil }
         })
         _ = try sanitiseRules(rules, text: c.chars)
      }())
      let d = "rule N : M, A : B, R : L, B : D, K : P, D : A"
      XCTAssertThrowsError(try {
         let text = try parse(text: d)
         let rules: [RulePack] = text.compactMap({
            if case let .rules(rp) = $0 { return rp }
            else { return nil }
         })
         _ = try sanitiseRules(rules, text: d.chars)
      }())
      let e = "rule A : C, B : C, C : D, C : E, D : A"
      XCTAssertThrowsError(try {
         let text = try parse(text: e)
         let rules: [RulePack] = text.compactMap({
            if case let .rules(rp) = $0 { return rp }
            else { return nil }
         })
         _ = try sanitiseRules(rules, text: e.chars)
      }())
      let f = "rule A : A"
      XCTAssertThrowsError(try {
         let text = try parse(text: f)
         let rules: [RulePack] = text.compactMap({
            if case let .rules(rp) = $0 { return rp }
            else { return nil }
         })
         _ = try sanitiseRules(rules, text: f.chars)
      }())
   }
   func testSemaCanFindDuplicateCtorNames() {
      _ = {
         let text = "data A : B | C | C"
         let rawitems = try! parse(text: text)
         let onlydata: [Sigil.Data] = rawitems.compactMap({
            if case .datadecl(let dt) = $0 { return dt }
            return nil
         })
         let dups = findDuplicateCtorNames(in: onlydata[0])
         XCTAssertNotNil(dups)
         XCTAssert(dups!.1[0].name.name == "C".chars)
      }()
      _ = {
         let text = "data A : B | C | D (a : R) | N | D (b : N)"
         let rawitems = try! parse(text: text)
         let onlydata: [Sigil.Data] = rawitems.compactMap({
            if case .datadecl(let dt) = $0 { return dt }; return nil
         })
         let dups = findDuplicateCtorNames(in: onlydata[0])
         XCTAssertNotNil(dups)
         XCTAssert(dups!.1[0].name.name == "D".chars)
      }()
   }
   func testSemaCanFindDuplicateDefinitions() {
      _ = {
         let b = "univ A, B, C\n" + "univ D, B\n"
         do {
            let env = try! parse(text: b)
            let fil: [UniPack] = env.compactMap({
               if case let .universe(dd) = $0 { return dd }
               return nil
            })
            switch sanitiseUniverseDecls(fil, text: b.chars) {
            case let .left(err) :
               XCTAssert(err[0].code == .dupReference)
            case .right :
               XCTAssert(false, "Should have failed but didnt!")
            }
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data 'K #' (A : U) : U | '1 + #' (n : U) | zero\n" +
         "fn 'K #' (a : U) : U\n" +
         "| _ => zero\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssert(err[0].code == .dupReference)
         case .right:
            XCTAssert(false, "Unxpected failure!")
         }
      }()
   }
   
   func testSemaCanFindClashesInLocalScope() {
      _ = {
         let t1 =
         "univ U\n" +
         "data 'K #' (A : U) : U | '1 + #' (n : U) | zero\n" +
         "fn 'K #' (a : U) : U\n" +
         "| `a => zero\n" +
         "| _ => zero\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssert(err[0].code == .dupReference)
         case .right:
            XCTAssert(false, "Unxpected failure!")
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data 'K #' (A : U) : U | '1 + #' (n : U) | zero\n" +
         "fn 'J #' (a : U) : U\n" +
         "| `b => zero\n" +
         "| _ => zero\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(false, "Should have succeeded but didnt!")
         case .right:
            XCTAssert(true)
         }
      }()
   }
   
   func testSemaAcceptsCorrectText() {
      _ = {
         let t1 =
         "univ A\n" +
         "data Dot : A | point\n" +
         "fn 'just #' (a : Dot) : Dot\n" +
         "| _ => point\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(false, "Should have succeeded but didnt!")
         case .right:
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (a : N, b : N) : N\n" +
         "| zero, `b => `b\n" +
         "| '1 + (`a)', `b => ?'(`a) + (`b)'\n" +
         "  | zero => zero\n" +
         "  | `c => '(`c) + (zero)'\n" +
         "| `a, _ => `a\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(false, "Unxpected failure!")
         case .right:
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (a : N, b : N) : N\n" +
         "| zero, `b => `b\n" +
         "| `k = '1 + (_)', `b => ?'(`k) + (`b)'\n" +
         "  | zero => zero\n" +
         "  | `c => '(`c) + (zero)'\n" +
         "| `b, _ => `b\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(false, "Unxpected failure!")
         case .right:
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ A\n" +
         "data Dot : A | point\n" +
         "fn 'just #' (a : Dot) : Dot\n" +
         "| point => point\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left(_):
            XCTAssert(false, "Unxpected failure!")
         case .right(_):
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ A\n" +
         "data Dot : A | point\n" +
         "fn 'just #' (a : Dot) : Dot\n" +
         "| point => point\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left(_):
            XCTAssert(false, "Unxpected failure!")
         case .right(_):
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ A, B, C, D\n" +
         "rule A : B, B : D, C : D\n" +
         "data N : A | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (a : N, b : N) : A\n" +
         "| _ , _ => '1 + (zero)'\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left(_):
            XCTAssert(false, "Unxpected failure!")
         case .right(_):
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (a : N, b : N) : N\n" +
         "| zero, `b => `b\n" +
         "| '1 + (`a)', `b => '(`a) + ('1 + (`b)')'\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(false, "Unxpected failure!")
         case .right(_):
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data A : U | Ctor1 | Ctor2\n" +
         "fn '# D #' (a : A, a : A) : A\n" +
         "| _, _ => A\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(true)
         case .right:
            XCTAssert(false, "Unxpected failure!")
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data A : U | Ctor1 | Ctor2\n" +
         "fn '# D #' {G : U} (a : G, b : G) : A\n" +
         "| _, _ => A\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(false, "Unxpected failure!")
         case .right:
            XCTAssert(true)
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data A : U | Ctor1 | Ctor2\n" +
         "fn '# D #' {G : U} (a : G, b : G) : A\n" +
         "| _, _ => Ctor1\n" +
         "def ref {M : U} : A = '(M) D (M)'\n" +
         "fn 'ex1 #' (a : A) : U | _ => ref\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case .left:
            XCTAssert(false, "Unxpected failure!")
         case .right:
            XCTAssert(true)
         }
      }()
   }
   
   func testSemaRejectsInvalidArgs() {
      _ = {
         let t1 =
         "univ U\n" +
         "data A : U | Ctor1 | Ctor2\n" +
         "fn '# D #' (a : A, a : A) : A\n" +
         "| _, _ => A\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssert(err[0].code == .duplicatesInArgBlock)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data A {A : U, A : U} : U | Ctor1 | Ctor2\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssert(err[0].code == .duplicatesInArgBlock)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data 'G # #' (A : U, A : U) : U | Ctor1 | Ctor2\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssert(err[0].code == .duplicatesInArgBlock)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
   }
   
   func testSemaRejectsIncorrectDeconstructions() {
      _ = {
         let t1 =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (a : A, b : B) : N\n" +
         "| zero, `b => `b\n" +
         "| '1 + (`a)', `b => ?'(`a) + (`b)'\n" +
         "  | zero, zero => zero\n" +
         "  | `c => '(`c) + (zero)'\n" +
         "| `a, _ => `a\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssert(err[0].code == .invalidPattern)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (a : A, b : B) : N\n" +
         "| zero, `b => `b\n" +
         "| '1 + (`a)', `b => ?'(`a) + (`b)'\n" +
         "  | zero => zero\n" +
         "  | `c => '(`c) + (zero)'\n" +
         "| `a => `a\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssert(err[0].code == .invalidPattern)
         case .right:
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
   }
   
   func testSemaDetectsInvalidFunlikeNames() {
      _ = {
         let t1 =
         "univ A\n" +
         "data J : A\n" +
         "data Dot (j : J) : A | point\n"
         let renv = try! parse(text: t1)
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .invalidFunlikeRef)
         case .right(_):
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ A\n" +
         "data Dot : A | point\n" +
         "fn 'f ' (a : Dot) : Dot | _ => point\n"
         let renv = try? parse(text: t1)
         guard let renv = renv else {
            XCTAssert(true, "Parsing failed")
            fatalError("I dont fall through")
         }
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .invalidFunlikeRef)
         case .right(_):
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
   }
   
   func testPrimitivesAreNotDisrupted() {
      _ = {
         let t1 =
         "univ A\n" +
         "data 'max # #' : A | point\n"
         let renv = try? parse(text: t1)
         guard let renv = renv else {
            XCTAssert(true, "Parsing failed")
            fatalError("I dont fall through")
         }
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .attemptToMessWithPrimitive)
         case .right(_):
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ A\n" +
         "fn 'max # #' (a : A, b : A) : A | _ => point\n"
         let renv = try? parse(text: t1)
         guard let renv = renv else {
            XCTAssert(true, "Parsing failed")
            fatalError("I dont fall through")
         }
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .attemptToMessWithPrimitive)
         case .right(_):
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
      _ = {
         let t1 =
         "univ 'max # #'\n"
         let renv = try? parse(text: t1)
         guard let renv = renv else {
            XCTAssert(true, "Parsing failed")
            fatalError("I dont fall through")
         }
         let rep = buildEnvironment(from: renv, text: t1.chars)
         switch rep {
         case let .left(err):
            XCTAssertTrue(err[0].code == .attemptToMessWithPrimitive)
         case .right(_):
            XCTAssert(false, "Should have failed but didnt!")
         }
      }()
   }
   func testPatternMatcher() {
//      _ = {
//         let repr = PatternExpression.Repr.compound(
//            "1+#".chars, [
//               .init(
//                  repr: .variable(.init(name: "a".chars, location: .fake)),
//                  location: .fake)])
//         let pattern = PatternExpression(repr: repr, location: .fake)
//         let exprepr = Expression.Repr.appl("1+#".chars, [
//            .init(expr: .appl("1+#".chars, [
//               .init(
//                  expr: .const(.init(name: "zero".chars, location: .fake)),
//                  location: .fake)]),
//                  location: .fake)])
//         let expr = Expression(expr: exprepr, location: .fake)
//         let outcome = suits(
//            exp: expr,
//            pattern: pattern,
//            parrent: FakeLocation(),
//            text: "".chars)
//         XCTAssert(outcome)
//      }()
//      _ = {
//         let repr = PatternExpression.Repr.compound(
//            "1+#".chars, [
//               .init(
//                  repr: .refrerence(.init(name: "zero".chars, location: .fake)),
//                  location: .fake)])
//         let pattern = PatternExpression(repr: repr, location: .fake)
//         let exprepr = Expression.Repr.appl("1+#".chars, [
//            .init(expr: .appl("1+#".chars, [
//               .init(
//                  expr: .const(.init(name: "zero".chars, location: .fake)),
//                  location: .fake)]),
//                  location: .fake)])
//         let expr = Expression(expr: exprepr, location: .fake)
//         let outcome = suits(
//            exp: expr,
//            pattern: pattern,
//            parrent: FakeLocation(),
//            text: "".chars)
//         XCTAssert(!outcome)
//      }()
   }
}
