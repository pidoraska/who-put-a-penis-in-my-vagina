//
//  File.swift
//  
//
//  Created on 23/11/21.
//

import XCTest
@testable import Sigil

class InterpreterTests : XCTestCase {
   
   func testInterpreterCanHandleNats() throws {
      _ = try {
         let ex =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (l : N, r : N) : N\n" +
         "| zero, `b => `b\n" +
         "| '1 + (`a)', `b => '(`a) + ('1 + (`b)')'\n" +
         "def two : N = '('1 + (zero)') + ('1 + (zero)')'\n"
         let renv = try? parse(text: ex)
         XCTAssert(renv != nil)
         let env = buildEnvironment(from: renv!, text: ex.chars)
         switch env {
         case .left:
            XCTAssert(false, "Unexpected failure")
         case let .right(s):
            let interpreter = Interpreter(enviroment: s, text: ex.chars)
            let natRedex = try interpreter.eval(definitionName: "two")
            XCTAssert(natRedex == "1 + (1 + (zero))")
         }
      }()
      _ = try {
         let ex =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (l : N, r : N) : N\n" +
         "| zero, `b => `b\n" +
         "| '1 + (`a)', `b => '(`a) + ('1 + (`b)')'\n" +
         "def two : N = '('1 + (zero)') + ('1 + (zero)')'\n" +
         "def four : N = '(two) + (two)'\n"
         let renv = try? parse(text: ex)
         XCTAssert(renv != nil)
         let env = buildEnvironment(from: renv!, text: ex.chars)
         switch env {
         case .left:
            XCTAssert(false, "Unexpected failure")
         case let .right(s):
            let interpreter = Interpreter(enviroment: s, text: ex.chars)
            let natRedex2 = try interpreter.eval(definitionName: "two")
            XCTAssert(natRedex2 == "1 + (1 + (zero))")
            let natRedex4 = try interpreter.eval(definitionName: "four")
            XCTAssert(natRedex4 == "1 + (1 + (1 + (1 + (zero))))")
         }
      }()
   }
   func testInterpreterOnBasicCases() throws {
      _ = try {
         let ex =
         "univ U\n" +
         "data N : U | '1 + #' (n : N) | zero\n" +
         "fn '# + #' (l : N, r : N) : N\n" +
         "| zero, `b => `b\n" +
         "| '1 + (`a)', `b => '(`a) + ('1 + (`b)')'\n" +
         "def two : N = '('1 + (zero)') + ('1 + (zero)')'\n" +
         "fn '# * #' (l:N,r:N):N\n" +
         "| '1 + (`a)' , `b => '(`b) + ('(`a) * (`b)')'\n" +
         "| zero, _ => zero\n" +
         "def four : N = '(two) * (two)'\n"
         let renv = try? parse(text: ex)
         XCTAssert(renv != nil)
         let env = buildEnvironment(from: renv!, text: ex.chars)
         switch env {
         case .left:
            XCTAssert(false, "Unexpected failure")
         case let .right(s):
            let interpreter = Interpreter(enviroment: s, text: ex.chars)
            let natRedex = try interpreter.eval(definitionName: "four")
            XCTAssert(natRedex == "1 + (1 + (1 + (1 + (zero))))")
         }
      }()
   }
}
