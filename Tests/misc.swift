//
//  File.swift
//  
//
//  Created on 20/11/21.
//

import XCTest
@testable import Sigil

class MiscellaneousTests : XCTestCase {
   func testHashabilityOfSymbol () {
      _ = {
         var set: Set<Symbol> = []
         let sym1 = Symbol(
            name: "abba".chars, location: .init(span: 1 ..< 3, line: 4))
         let sym2 = Symbol(
            name: "baab".chars, location: .init(span: 11 ..< 13, line: 14))
         set.insert(sym1)
         set.insert(sym2)
         XCTAssertTrue(set.count == 2)
      }()
      _ = {
         var set: Set<Symbol> = []
         let sym1 = Symbol(
            name: "abba".chars, location: .init(span: 1 ..< 3, line: 4))
         let sym2 = Symbol(
            name: "abba".chars, location: .init(span: 11 ..< 13, line: 14))
         set.insert(sym1)
         set.insert(sym2)
         XCTAssertTrue(set.count == 1)
      }()
   }
}
