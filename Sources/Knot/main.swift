//
//  main.swift
//  casper-pragma
//
//  Created on 17/11/21.
//

import Sigil
import Foundation

let control =  "\u{1b}", dim = control + "[2m", done = control + "[0m",
    logofgcolour = control + "[38;5;203m", linecolour = control + "[38;5;238m"
let padding = String(repeating: " ", count: 25)
let logo =
"""
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
██ ▄▄▄ █▄ ▄██ ▄▄ █▄ ▄██ ████
██▄▄▄▀▀██ ███ █▀▀██ ███ ████
██ ▀▀▀ █▀ ▀██ ▀▀▄█▀ ▀██ ▀▀██
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
"""

var delimeter : String? = nil
var lengthOfFirstLineOfHeader : Int? = nil
let greatingHeader = { () -> String in
   let spl = logo.split(separator: "\n")
   lengthOfFirstLineOfHeader = spl[0].count
   delimeter = String(
      repeating: "–", count: padding.count * 2 + lengthOfFirstLineOfHeader!) + "\n"
   return spl.map({ padding + $0 + padding + "\n" }).joined()
}()
let v =  "Welcome to Sigilᵦ 0.1"
let version =
   padding + String(repeating: " ", count: (lengthOfFirstLineOfHeader! - v.count) / 2)
   + v

let eraseCode = control + "[2K\r"
let putCurOneSymLeft = control + "[1D"

func clearLastLines(_ count: Int) {
   var i = 0
   while i != count {
      print(eraseCode, terminator: "")
      print(putCurOneSymLeft, terminator: "")
      i += 1
   }
}
let ptrcol = control + "[38;5;140m"

let header =
   linecolour + delimeter! + done + logofgcolour + greatingHeader + done +
   dim + version + "\n" + done +
   linecolour + delimeter! + done
print(header, terminator: "\n")

func contract (_ str: String) -> String {
   let forbiddenChars = "\n "
   return str.filter({ char in !forbiddenChars.map(Character.init).contains(char) })
}

let promptcol = control + "[38;5;214m"
let hintcol = control + "[38;5;28m"
let errcol = control + "[38;5;126m"

var environment : Env = [:]
var storage: String = ""

while true {
   print(promptcol + " Awaiting your command now." + done, terminator: "\n")
   var line: String = ""
   while true {
      print(ptrcol + ">" + done, terminator: " ")
      if let ln = readLine(strippingNewline: true) {
         if ln == "" { continue }
         line = ln; break
      } else { continue }
   }
   
   let trimmed = line.trimmingCharacters(in: .whitespacesAndNewlines)
   
   if trimmed == ":new-def" {
      runDeclContext()
      continue
   }
   if trimmed.hasPrefix(":eval") {
      let arg = trimmed.dropFirst(5).trimmingCharacters(in: .whitespaces)
      let defn = environment[Symbol(name: arg.chars, location: .fake)]
      guard let defn = defn as? Definition else {
         print(errcol + "Environment doesnt contain \(arg)" + done, terminator: "\n\n")
         continue
      }
      do {
         let outcome = try rewrite(
            expression: defn.value, env: environment, text: storage.chars)
         let repr = renderAsText(outcome)
         print("\n" + " " + hintcol + repr + done, terminator: "\n\n")
      } catch let err as EvaluationProblem {
         print(err.msg, terminator: "\n\n")
      }
      continue
   }
      
   print(hintcol + "  \(line) is not a known command" + done, terminator: "\n\n")
   continue
}

func runDeclContext () {
   print("\n", terminator: "")
   print(promptcol + " Ready to accept your definition now." + done, terminator: "\n")
   var localText : [String] = []
   while true {
      print(dim + "|>" + done, terminator: " ")
      guard let nl = readLine(strippingNewline: false) else {
         fatalError("suddenly havent recieved anything from input at all")
      }
      if nl == ":done\n" {
         print("\n", terminator: "")
         break
      }
      localText.append(nl)
   }
   do {
      let text = storage + localText.joined()
      if text.isEmpty { return }
      let renv = try parse(text: text)
      let env = buildEnvironment(from: renv, text: text.chars)
      switch env {
      case let .left(problems):
         let all = problems.map({ $0.msg }).joined(separator: "\n\n")
         print(errcol + all + done, terminator: "\n\n")
      case let .right(env):
         environment = env
         storage = text
         print(hintcol + " Successfully added" + done, terminator: "\n\n")
      }
   } catch let err {
      if case .syntacticError(let msg) = err as? ParseFailure {
         print(errcol + msg + done, terminator: "\n\n")
      }
   }
}
