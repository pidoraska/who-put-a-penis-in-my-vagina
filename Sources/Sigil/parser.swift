//
//  parser.swift
//  sigil
//
//  Created on 16/11/21.
//

import CloudKit

public protocol Locateable { var location: SourceLocation { get } }
public protocol ExpressionHolder {
   var localEnv: [(Symbol, Expression, ArgKind)] { get } }
public enum ParseFailure: Error {
   case syntacticError(String), nothingToParse
}
public struct SourceLocation {
   let span: Range<Int>, line:Int
}
public struct ImplicitArgs: Locateable {
   public let args: [(sym: Symbol, tvar: Expression)], location: SourceLocation
   var parentDecl: Any
}
public struct ExplicitArgs: Locateable {
   public let args: [(sym: Symbol, tvar: Expression)], location: SourceLocation
   var parentDecl: Any
}
public struct Symbol: Locateable {
   public let name: [Character], location: SourceLocation
   //var parentDecl: Any
   public init(name: [Character], location: SourceLocation) {
      self.name = name.filter({ $0 != "\n" && $0 != " " })
      self.location = location
   }
}
extension Symbol {
   public var inpArgCount: Int {
      var num = 0, ptr = 0
      while ptr != name.endIndex {
         if name[ptr] == "#" { num += 1 }
         ptr += 1
      }
      return num
   }
}
extension SourceLocation: Hashable, Equatable {}
extension Symbol : Hashable, Equatable {
   public static func == (l: Self, r: Self) -> Bool {
      l.name == r.name
   }
   public func hash(into hasher: inout Hasher) { hasher.combine(self.name) }
}
extension Symbol : CustomStringConvertible {
   public var description: String { String(self.name) }
}
func ~= <T: Collection, P: Collection>(target: T, pattern: P) -> Bool
where T.Element == P.Element, T.Element : Equatable {
   if target.count != pattern.count { return false }
   var titer = target.makeIterator(), piter = pattern.makeIterator()
   while true {
      guard let p = piter.next() else { return true }
      guard let a = titer.next() else { return false }
      if a == p { continue }
      return false
   }
}
public struct Data: Locateable {
   public struct Constructor: Locateable {
      public let name: Symbol, args: [(Symbol, Expression)], location: SourceLocation
      var parentDecl: Any
   }
   public var name: Symbol, impArgs: ImplicitArgs?, expArgs: ExplicitArgs?,
       ctors: [Constructor], location: SourceLocation, ulevexpr: Expression
}
extension Data {
   public var localEnv: [(Symbol, Expression, ArgKind)] {
      var res : [(Symbol, Expression, ArgKind)] = []
      res += self.impArgs?.args.map({ (a,b) in (a,b,.implicit) }) ?? []
      res += self.expArgs?.args.map({ (a,b) in (a,b,.explicit) }) ?? []
      return res
   }
}
extension Data: ExpressionHolder {}
extension Data.Constructor : Equatable, Hashable {
   public static func == (lhs: Data.Constructor, rhs: Data.Constructor) -> Bool {
      lhs.name == rhs.name
   }
   public func hash(into hasher: inout Hasher) {
      hasher.combine(name)
   }
}
public struct Definition: Locateable {
   public var name: Symbol, implicitArgs: ImplicitArgs?, value: Expression,
              location: SourceLocation, type: Expression
}
extension Definition : ExpressionHolder {
   public var localEnv: [(Symbol, Expression, ArgKind)] {
      implicitArgs?.args.map({ (a,b) in (a,b,.implicit) }) ?? []
   }
}
extension Definition: Equatable, Hashable {
   public static func == (lhs: Definition, rhs: Definition) -> Bool {
      lhs.name == rhs.name
   }
   public func hash(into hasher: inout Hasher) {
      hasher.combine(name)
   }
}
extension Data : Equatable {
   public static func == (lhs: Data, rhs: Data) -> Bool {
      lhs.name.name == rhs.name.name
   }
}
extension Data : Hashable {
   public func hash(into hasher: inout Hasher) {
      hasher.combine(name)
   }
}
public struct Prototype {
   struct Ty {}
   let name: Symbol
   let signature: Ty
}
public struct Mapping: Locateable {
   public var location: SourceLocation, reference: Symbol, patterns: [PatternClause],
       implicitArgs: ImplicitArgs?, explicitArgs: ExplicitArgs, outputDomain: Expression
}
public struct FakeLocation: Locateable {
   public var location: SourceLocation { .fake }
}
extension Mapping : Equatable, Hashable {
   public static func == (lhs: Mapping, rhs: Mapping) -> Bool {
      lhs.reference == rhs.reference
   }
   public func hash(into hasher: inout Hasher) {
      hasher.combine(reference)
   }
}
public enum ArgKind { case implicit, explicit }
extension Mapping {
   public var localEnv: [(Symbol, Expression, ArgKind)] {
      var res : [(Symbol, Expression, ArgKind)] = []
      res += self.implicitArgs?.args.map({ (a,b) in (a,b,.implicit) }) ?? []
      res += self.explicitArgs.args.map({ (a,b) in (a,b,.explicit) })
      return res
   }
}
extension Mapping: ExpressionHolder {}
public enum RawItem {
   case datadecl(Data), protodecl(Prototype), universe(UniPack),
        rules(RulePack), mapping(Mapping), defn(Definition)
}
public struct UniPack: Locateable {
   public let location: SourceLocation, unis: [Symbol]
}
public struct RulePack: Locateable {
   public let location: SourceLocation, rules: [(Symbol,Symbol)]
}
public struct PatternExpression: Locateable {
   public indirect enum Repr {
           // _
      case wildcard,
           // def or data or uni
           refrerence(Symbol),
           // 'A (B)'
           compound([Character], [PatternExpression]),
           // k = 'A (B)'
           tagged(Symbol, PatternExpression),
           // `a
           variable(Symbol) //,imppat
   }
   public let repr: Repr, location: SourceLocation
}
public struct PatternClause: Locateable {
   public indirect enum Repr {
           // | <pats> => <expr>
      case monopat(Expression),
           // | <pats> => ? <exps> \n | ...
           chainpat([Expression], [PatternClause])
   }
   public let location: SourceLocation, rhs: Repr, stencil: [PatternExpression]
}

extension String {
   public var chars: [Character] {
      self.unicodeScalars.map(Character.init)
   }
}
public func == <A,B>(l: A, r: B) -> Bool
where A: Collection, B: Collection, A.Element == B.Element, A.Element: Equatable {
   l ~= r
}
public struct Expression: Locateable {
   public enum Repr {
      case appl([Character], [Expression]), const(Symbol), variable(Symbol)
   }
   public let expr: Repr, location: SourceLocation
}
extension Expression {

   public var mentionedVariables: Set<Symbol> {
      var set: Set<Symbol> = []
      func rec(_ v: Expression) {
         switch v.expr {
         case let .appl(_, exps): for exp in exps { rec(exp) }
         case .const(_): ()
         case let .variable(sym): set.insert(sym)
         }
      }
      rec(self)
      return set
   }
}

public func parse(text: String) throws -> [RawItem] {
   enum ParseState { case data, seeking, rule, univ, fn, def }
   if text.isEmpty { throw ParseFailure.nothingToParse }
   var chars = text.unicodeScalars.map(Character.init), eot: Character = "\u{3}"
   chars.append(eot)
   var ptr: Int = 0, result: [RawItem] = [], mostRecentNewlinePosition: Int = 0,
       parseState = ParseState.seeking, currentLineNumber: Int = 1,
       mostRecentTopLevelDeclStart: Int = 0
   var currentCharacter: Character { chars[ptr] }
   var isDone : Bool { currentCharacter == eot && parseState == .seeking }
   var isHanging: Bool { currentCharacter == eot && parseState != .seeking }
   var currentLineLength: Int { ptr - mostRecentNewlinePosition }
   var errorHeader: String {
      let charinfo = currentCharacter.unicodeScalars.reduce(into: "", {
         $0.append("\\u" + $1.value.description)
      })
      let helper =
      "Parsing stoped at character '\(currentCharacter)' (codepoint:\(charinfo))" + "\n"
      let hcou = helper.count
      let count = ptr - mostRecentNewlinePosition - (currentLineNumber > 1 ? 1 : 0)
      let back = ptr
      try? skip(while: {
         if currentCharacter == "\n" || currentCharacter == eot { return false }
         return true
      })
      let nlp = ptr
      ptr = back
      let erh =
      "Error at line \(currentLineNumber), " + "column \(count).\n" + helper +
      String(repeating: "-", count: hcou) + "\n" +
      chars[mostRecentTopLevelDeclStart ..< nlp] + "\n"
      let str = String(repeating: " ", count: count) + "^" + "\n"
      return erh + str
   }
   var unxpectedCharErr: ParseFailure {
      .syntacticError(errorHeader + "Unexpected '\(currentCharacter)' here") }
   func skip(while condition: () -> Bool) throws {
      while true {
         if condition() {
            if chars[ptr] == eot {
               let err = errorHeader + "File unexpectedly ended"
               throw ParseFailure.syntacticError(err)
            }
            if currentCharacter == "\n" {
               mostRecentNewlinePosition = ptr; currentLineNumber += 1 }
            ptr += 1
         } else { break }
      }
   }
   func skip(_ count: Int) throws {
      let newPtr = count + ptr
      if newPtr >= chars.endIndex {
         let err = errorHeader +
         "Attempted to skip more characters than present in text"
         throw ParseFailure.syntacticError(err)
      }
      var of = 0
      for char in chars[ptr ... newPtr] {
         if char == "\n" { mostRecentNewlinePosition += of; currentLineNumber += 1 }
         of += 1
      }
      ptr = newPtr
   }
   func skipDelimiters() {
   this:while true {
      switch currentCharacter {
      case "\n" : mostRecentNewlinePosition = ptr; ptr += 1; currentLineNumber += 1
      case " " : ptr += 1
      default: break this
      }}
   }
   func skipWhitespaces() { while currentCharacter == " " { ptr += 1 } }
   func skipNewlines() {
      while currentCharacter == "\n" {
         mostRecentNewlinePosition = ptr; currentLineNumber += 1; ptr += 1
      }
   }
   func fscan () throws {
      skipDelimiters()
      let priorHead = ptr
      defer {
         ptr = priorHead
         mostRecentTopLevelDeclStart = priorHead
      }
      var limit = 0
      try skip(while: {
         if limit == 4 || !currentCharacter.isLetter { return false }
         limit += 1
         return true
      })
      switch chars[priorHead ..< ptr] {
      //fix me : use bin tree
      case "data": parseState = .data
      case "univ": parseState = .univ
      case "rule": parseState = .rule
      case "fn": parseState = .fn
      case "def": parseState = .def
      default:
         let err = errorHeader + "Expected keyword but found " +
         "'\(String(chars[priorHead ... ptr]))'\n"
         throw ParseFailure.syntacticError(err)
      }
   }
   func parseSymbol () throws -> Symbol {
      skipDelimiters()
      let start = ptr
      let line = currentLineNumber
      if currentCharacter == "'" {
         ptr += 1
         while true {
            if currentCharacter == "'" { break }
            guard currentCharacter != "(" && currentCharacter != ")" else {
               let err = errorHeader +
               "\(currentCharacter) is a reserved symbol"
               throw ParseFailure.syntacticError(err)
            }
            if currentCharacter.isLetter || currentCharacter.isNumber
                  || currentCharacter.isMathSymbol || currentCharacter.isSymbol
                  || currentCharacter.isPunctuation || currentCharacter == " "
            {
               if currentCharacter == "\n" {
                  mostRecentNewlinePosition = ptr; currentLineNumber += 1
               }
               ptr += 1
            } else {
               throw unxpectedCharErr
            }
         }
         let end = ptr
         if (end - start) == 1 {
            let err = errorHeader + "Empty names are forbidden"
            throw ParseFailure.syntacticError(err)
         }
         ptr += 1
         return Symbol(
            name: Array(chars[(start + 1) ..< end]),
            location: .init(span: start ..< ptr, line: line))
      } else {
         try skip(while: { currentCharacter.isLetter || currentCharacter.isNumber })
      }
      let end = ptr
      if (end - start) == 0 {
         let err = errorHeader + "A valid symbol is expected here"
         throw ParseFailure.syntacticError(err)
      }
      //fix me : remove array realloc
      return Symbol(
         name: Array(chars[start ..< end]),
         location: .init(span: start ..< end, line: line))
   }
   func parseExpr() throws -> Expression {
      skipDelimiters()
      let exprStartPoint = ptr
      let line = currentLineNumber
      
      if currentCharacter == "`" {
         ptr += 1
         let vr = try parseSymbol()
         let loc = SourceLocation.init(span: exprStartPoint ..< ptr, line: line)
         return .init(expr: .variable(vr), location: loc)
      }
      if currentCharacter == "'" {
         ptr += 1
         var symb: [Character] = [], args: [Expression] = []
         while true {
            switch currentCharacter {
            case "(":
               ptr += 1
               symb.append("#")
               let subexp = try parseExpr()
               args.append(subexp)
               skipDelimiters()
               guard currentCharacter == ")" else {
                  print(currentCharacter)
                  let err = errorHeader +
                  "Subexpression is not terminated by a )"
                  throw ParseFailure.syntacticError(err)
               }
               ptr += 1
            case "'":
               ptr += 1
               let exprEndPoint = ptr
               let loc = SourceLocation(
                  span: exprStartPoint ..< exprEndPoint, line: line)
               if args.isEmpty {
                  let symbol = Symbol(name: symb, location: loc)
                  let exp = Expression(expr: .const(symbol), location: loc)
                  return exp
               }
               symb = symb.filter({ $0 != "\n" || $0 != " " })
               return .init(expr: .appl(symb, args), location: loc)
            default:
               symb.append(currentCharacter)
               ptr += 1
            }
         }
      }
      let sym = try parseSymbol()
      let loc = SourceLocation.init(span: exprStartPoint ..< ptr, line: line)
      return .init(expr: .const(sym), location: loc)
   }
   func parseArgsBlock() throws -> [(Symbol, Expression)] {
      enum State { case tvar, expr }
      let temrChar: Character = currentCharacter == "{" ? "}" : ")"
      ptr += 1
      var impArgs: [(sym: Symbol, expr: Expression)] = []
      skipDelimiters()
      var compoundVars : [Symbol] = [], state = State.tvar
   l:while true {
      switch currentCharacter {
      case ":":
         guard state == .tvar else { throw unxpectedCharErr }
         if compoundVars.isEmpty { throw unxpectedCharErr }
         state = .expr
         ptr += 1
         skipDelimiters()
         let expr = try parseExpr()
         for sym in compoundVars {
            impArgs.append((sym, expr))
         }
         compoundVars = []
         skipDelimiters()
         if currentCharacter == temrChar { ptr += 1; break l }
         if currentCharacter != "," { throw unxpectedCharErr }
      case ",":
         guard state == .expr else { throw unxpectedCharErr }
         ptr += 1
         state = .tvar
      case temrChar:
         guard state != .tvar else { throw unxpectedCharErr }
         if impArgs.isEmpty {
            let err = errorHeader + "Arguments block is empty"
            throw ParseFailure.syntacticError(err)
         }
         ptr += 1; break l
      default:
         let sym = try parseSymbol()
         compoundVars.append(sym)
         skipDelimiters()
         continue l
      }
   }
      return impArgs
   }
   func parseCtor() throws -> Data.Constructor {
      let declBegin = ptr
      let line = currentLineNumber
      ptr += 1
      let name = try parseSymbol()
      skipDelimiters()
      if currentCharacter == "(" {
         let args = try parseArgsBlock()
         let declEnd = ptr
         return .init(
            name: name, args: args,
            location: .init(span: declBegin ..< declEnd, line: currentLineNumber),
            parentDecl: ())
      }
      let declEnd = ptr
      return .init(
         name: name, args: [], location: .init(span: declBegin ..< declEnd, line: line),
         parentDecl: ())
   }
   func parseDataDecl () throws -> Data {
      enum State { case none, imps, exps, root, ctor }
      defer { parseState = .seeking }
      skipDelimiters()
      let beginOfDataDecl = ptr
      let line = currentLineNumber
      ptr += 4
      skipDelimiters()
      let name: Symbol = try parseSymbol()
      skipDelimiters()
      var state = State.none, imps: ImplicitArgs? = nil, exps: ExplicitArgs? = nil,
          ctors: [Data.Constructor] = []
      var lev: Expression? = nil
   l:while true {
      switch currentCharacter {
      case "{":
         guard state == .none else { throw unxpectedCharErr }
         state = .imps
         let declBegin = ptr
         let implArgsBlock = try parseArgsBlock()
         let declEnd = ptr
         imps = .init(
            args: implArgsBlock,
            location: .init(span: declBegin ..< declEnd, line: currentLineNumber),
            parentDecl: ())
         skipDelimiters()
      case "(":
         guard state == .none || state == .imps else { throw unxpectedCharErr }
         state = .exps
         let beginDecl = ptr
         let explArgsBlock = try parseArgsBlock()
         let endDecl = ptr
         exps = .init(
            args: explArgsBlock,
            location: .init(span: beginDecl ..< endDecl, line: currentLineNumber),
            parentDecl: ())
         skipDelimiters()
      case ":":
         guard state == .none || state == .exps || state == .imps && state != .root
         else { throw unxpectedCharErr }
         state = .root
         ptr += 1
         lev = try parseExpr()
         skipDelimiters()
      case "|":
         guard state == .root || state == .ctor else { throw unxpectedCharErr }
         state = .ctor
         ctors.append(try parseCtor())
         skipDelimiters()
      default:
         guard state == .root || state == .ctor else { throw unxpectedCharErr }
         break l
      }
   }
      let endOfDataDecl = ptr
      let sloc = SourceLocation.init(span: beginOfDataDecl ..< endOfDataDecl, line: line)
      var result = Data(
         name: name, impArgs: imps, expArgs: exps, ctors: ctors,
         location:sloc, ulevexpr: lev!)
      if result.expArgs != nil {
         result.expArgs!.parentDecl = result
      }
      if result.impArgs != nil {
         result.impArgs!.parentDecl = result
      }
      for var ctor in result.ctors {
         ctor.parentDecl = result
      }
      return result
   }
   func parseRules() throws -> RulePack {
      defer { parseState = .seeking }
      let declStart = ptr
      ptr += 4
      skipDelimiters()
      var rules: [(Symbol,Symbol)] = []
      while true {
         let lhsu = try parseSymbol()
         skipDelimiters()
         guard currentCharacter == ":" else {
            let err = errorHeader +
            "Expected to find ':' here, but found '\(currentCharacter)' instead"
            throw ParseFailure.syntacticError(err)
         }
         ptr += 1
         let rhsu = try parseSymbol()
         rules.append((lhsu, rhsu))
         skipDelimiters()
         if currentCharacter == "," { ptr += 1; continue }
         else { break }
      }
      let loc = SourceLocation(span: declStart ..< ptr, line: currentLineNumber)
      return .init(location: loc, rules: rules)
   }
   func parseUniverseDecl() throws -> UniPack {
      defer { parseState = .seeking }
      let declStart = ptr
      let line = currentLineNumber
      ptr += 4
      skipDelimiters()
      var names: [Symbol] = []
      while true {
         let sym = try parseSymbol()
         names.append(sym)
         skipDelimiters()
         if currentCharacter == "," { ptr += 1; continue }
         else { break }
      }
      let loc = SourceLocation(span: declStart ..< ptr, line: line)
      return .init(location: loc, unis: names)
   }
   func parseTaggedClauseExpr() throws -> PatternExpression? {
      skipDelimiters()
      let backtrackPosition = ptr
      let ln = currentLineNumber
      guard currentCharacter == "`" else { return nil }
      ptr += 1
      let ref = try parseSymbol()
      skipDelimiters()
      if currentCharacter == "=" && chars[ptr + 1] != ">" {
         ptr += 1
         let patt = try parseMatchClausePattern()
         let loc = SourceLocation(span: backtrackPosition ..< ptr, line: ln)
         return .init(repr: .tagged(ref, patt), location: loc)
      } else {
         ptr = backtrackPosition
         return nil
      }
   }
   func parseMatchClausePattern() throws -> PatternExpression {
      skipDelimiters()
      let exprStartPoint = ptr
      if currentCharacter == "_" {
         ptr += 1
         let loc = SourceLocation(span: exprStartPoint ..< ptr, line: currentLineNumber)
         return .init(repr: .wildcard, location: loc)
      }
      // impossible pattern
      let tempptr = ptr, tempnl = currentLineNumber, tempmrnlp = mostRecentNewlinePosition
      let maybeTagged = try? parseTaggedClauseExpr()
      if let tagged = maybeTagged { return tagged }
      ptr = tempptr; currentLineNumber = tempnl; mostRecentNewlinePosition = tempmrnlp
      if currentCharacter == "`" {
         ptr += 1
         let loc = SourceLocation(span: exprStartPoint ..< ptr, line: currentLineNumber)
         let vr = try parseSymbol()
         return .init(repr: .variable(vr), location: loc)
      }
      if currentCharacter == "'" {
         ptr += 1
         var symb: [Character] = [], subexprs: [PatternExpression] = []
         let ln = currentLineNumber
         while true {
            switch currentCharacter {
            case "(":
               ptr += 1
               symb.append("#")
               let subexp = try parseMatchClausePattern()
               subexprs.append(subexp)
               skipDelimiters()
               guard currentCharacter == ")" else {
                  let err = errorHeader +
                  "Subexpression is not terminated by a )"
                  throw ParseFailure.syntacticError(err)
               }
               ptr += 1
            case "'":
               ptr += 1
               let exprEndPoint = ptr
               let loc = SourceLocation.init(
                  span: exprStartPoint ..< exprEndPoint, line: ln)
               if subexprs.isEmpty {
                  let symbol = Symbol(name: symb, location: loc)
                  return .init(repr: .refrerence(symbol), location: loc)
               }
               symb = symb.filter({ $0 != " " && $0 != "\n" })
               return .init(repr: .compound(symb, subexprs), location: loc)
            default:
               symb.append(currentCharacter)
               ptr += 1
            }
         }
      }
      let sym = try parseSymbol()
      let loc = SourceLocation.init(span: exprStartPoint ..< ptr, line: currentLineNumber)
      return .init(repr: .refrerence(sym), location: loc)
   }
   func parseSubpatterns(depth: Int) throws -> [PatternClause] {
      var subpattens: [PatternClause] = []
      while true {
         if ptr + depth >= chars.endIndex || isDone { return subpattens }
         let rollback = ptr
         ptr += depth
         if currentCharacter != "|" {
            ptr = rollback
            return subpattens
         }
         let subexpr = try parseMatchClause(minimumDelimSize: depth)
         subpattens.append(subexpr)
      }
   }
   func parseMatchClause(minimumDelimSize: Int) throws -> PatternClause {
      let clauseStart = ptr
      let line = currentLineNumber
      ptr += 1
      var patternExpr : [PatternExpression] = []
   loop:while true {
      let pm = try parseMatchClausePattern()
      patternExpr.append(pm)
      skipDelimiters()
      if currentCharacter == "," { ptr += 1; continue }
      if chars[ptr ... ptr + 1] == "=>" {
         ptr += 2
         skipDelimiters()
         if currentCharacter == "?" {
            ptr += 1
            var items: [Expression] = []
         i:while true {
            let cap = try parseExpr()
            items.append(cap)
            try skip(while: { currentCharacter == " " })
            if currentCharacter == "," { ptr += 1; continue i }
            if currentCharacter == "\n" {
               try skip(while: { currentCharacter == "\n" }); break i
            }
            throw unxpectedCharErr
         }
            let s = ptr
            try skip(while: { currentCharacter == " " })
            let e = ptr
            if e - s <= minimumDelimSize {
               let err = errorHeader +
               "Non increasingly indented clause within multipattern"
               throw ParseFailure.syntacticError(err)
            }
            ptr = s
            let patternSubexps = try parseSubpatterns(depth: e - s)
            let declEnd = ptr
            let loc = SourceLocation(span: clauseStart ..< declEnd, line: line)
            //change
            return .init(
               location: loc,
               rhs: .chainpat(items, patternSubexps),
               stencil: patternExpr)
         } else {
            let ln = currentLineNumber
            let evexp = try parseExpr()
            let declEnd = ptr
            try skip(while: { currentCharacter == " " })
            guard currentCharacter == "\n" else {
               let err = errorHeader +
               "Expressions in clauses must have a trailing newline"
               throw ParseFailure.syntacticError(err)
            }
            try skip(while: { currentCharacter == "\n" })
            let loc = SourceLocation(span: clauseStart ..< declEnd, line: ln)
            return .init(
               location: loc,
               rhs: .monopat(evexp),
               stencil: patternExpr)
         }
      }
      throw unxpectedCharErr
   }
   }
   func parseMapping() throws -> Mapping {
      enum State { case begin, imp, exp, root, clause }
      defer { parseState = .seeking }
      let declBegin = ptr
      let line = currentLineNumber
      ptr += 2
      let name = try parseSymbol()
      skipDelimiters()
      var state = State.begin
      var implBlock: ImplicitArgs? = nil, explBlock: ExplicitArgs? = nil,
          texp: Expression? = nil, clauses: [PatternClause] = [],
          firstClausePadding = 0
   loop:while true {
      switch currentCharacter {
      case "{" :
         guard state == .begin else { throw unxpectedCharErr }
         state = .imp
         let beg = ptr
         let ln = currentLineNumber
         let args = try parseArgsBlock()
         implBlock = .init(
            args: args, location: .init(span: beg ..< ptr, line: ln), parentDecl: ())
      case "(":
         guard state == .begin || state == .imp else { throw unxpectedCharErr }
         state = .exp
         let beg = ptr
         let ln = currentLineNumber
         let args = try parseArgsBlock()
         explBlock = .init(
            args: args, location: .init(span: beg ..< ptr, line: ln), parentDecl: ())
      case ":":
         guard state == .imp || state == .exp || state == .begin else {
            throw unxpectedCharErr
         }
         state = .root
         ptr += 1
         texp = try parseExpr()
         skipNewlines()
         let begin = ptr
         skipWhitespaces()
         firstClausePadding = ptr - begin
      case "|":
         guard state == .root || state == .clause else { throw unxpectedCharErr }
         state = .clause
         let clause = try parseMatchClause(minimumDelimSize: firstClausePadding)
         clauses.append(clause)
      default:
         guard state == .root || state == .clause else { throw  unxpectedCharErr }
         break loop
      }
      skipDelimiters()
   }
      if explBlock == nil {
         let err = errorHeader +
         "Arguments must be present in definition of \(name)"
         throw ParseFailure.syntacticError(err)
      }
      if clauses.isEmpty {
         let err = errorHeader +
         "Function \(name) must have at least one pattern clause"
         throw ParseFailure.syntacticError(err)
      }
      let declEnd = ptr
      let loc = SourceLocation(span: declBegin ..< declEnd, line: line)
      var result = Mapping(
         location: loc, reference: name, patterns: clauses,
         implicitArgs: implBlock, explicitArgs: explBlock!,
         outputDomain: texp!)
      result.explicitArgs.parentDecl = result
      if result.implicitArgs != nil {
         result.implicitArgs!.parentDecl = result
      }
      return result
   }
   func parseDef() throws -> Definition {
      defer { parseState = .seeking }
      let declBegin = ptr
      let line = currentLineNumber
      ptr += 3
      skipDelimiters()
      let name = try parseSymbol()
      skipDelimiters()
      var implicitArgs: ImplicitArgs? = nil
      if currentCharacter == "{" {
         let beg = ptr
         let ln = currentLineNumber
         let imps = try parseArgsBlock()
         let end = ptr
         let loc = SourceLocation(span: beg ..< end, line: ln)
         implicitArgs = .init(args: imps, location: loc, parentDecl: ())
      }
      skipDelimiters()
      guard currentCharacter == ":" else { throw unxpectedCharErr }
      ptr += 1
      skipDelimiters()
      let texp = try parseExpr()
      skipDelimiters()
      guard currentCharacter == "=" else { throw unxpectedCharErr }
      ptr += 1
      skipDelimiters()
      let expr = try parseExpr()
      let declEnd = ptr
      let loc = SourceLocation(span: declBegin ..< declEnd, line: line)
      var result = Definition(
         name: name, implicitArgs: implicitArgs, value: expr, location: loc, type: texp)
      if result.implicitArgs != nil {
         result.implicitArgs!.parentDecl = result
      }
      return result
   }
   while true {
      skipDelimiters()
      if isDone { return result }
      switch parseState {
      case .seeking: try fscan()
      case .data:
         let dataDecl = try parseDataDecl()
         result.append(.datadecl(dataDecl))
      case .rule:
         let rules = try parseRules()
         result.append(.rules(rules))
      case .univ:
         let unis = try parseUniverseDecl()
         result.append(.universe(unis))
      case .fn:
         let fun = try parseMapping()
         result.append(.mapping(fun))
      case .def:
         let def = try parseDef()
         result.append(.defn(def))
      }
   }
}
