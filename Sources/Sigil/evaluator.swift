//
//  File.swift
//  
//
//  Created on 21/11/21.
//


extension PatternClause {
   public var bindingPoints: Set<Symbol> {
      var vars: Set<Symbol> = []
      func recurseOnClause (_ pat: PatternClause) {
         for subc in pat.stencil { recurseOnPatExpr(subc) }
         switch pat.rhs {
         case let .chainpat(_, subpats):
            for subpat in subpats { recurseOnClause(subpat) }
         default: ()
         }
      }
      func recurseOnPatExpr(_ expr: PatternExpression) {
         switch expr.repr {
         case let .compound(_, pexprs): for pexp in pexprs { recurseOnPatExpr(pexp) }
         case let .tagged(_, pat): recurseOnPatExpr(pat)
         case let .variable(vari): vars.insert(vari)
         default: ()
         }
      }
      recurseOnClause(self)
      return vars
   }
}
public func testExactEquality(exp1: Expression, exp2: Expression) -> Bool {
   guard exp1.isRedex && exp2.isRedex else {
      fatalError("unreduced expression tested for equiality")
   }
   var result = true
   func rec(_ ex1: Expression, _ ex2: Expression) {
      switch (exp1.expr, exp2.expr) {
      case let (.appl(r1, exps), .appl(r2, exps2)):
         if r1 != r2 { result = false; return }
         for (e1, e2) in zip(exps, exps2) {
            rec(e1, e2)
            if result == false { return }
         }
      case let (.const(r), .const(r2)):
         if r != r2 { result = false }
      default: result = false
      }
   }
   rec(exp1, exp2)
   return result
}
public enum EvalProblemCode {
   case unreducedExpression, bindFail, oops, matchFailure, lookupFailure
}
public struct EvaluationProblem :Error {
   public let msg: String, code: EvalProblemCode
}

public func rewrite(
   expression: Expression, env: Env, text: [Character])
throws -> Expression {
   func recurse(_ expr: Expression)
   throws -> Expression {
      switch expr.expr {
      case let .appl(ref, exps):
         if let reductionScheme = env[Symbol(name: ref, location: .fake)] as? Mapping {
//            let denv = reductionScheme.localEnv.map({(a, b, _) in (a, b)})
//            let dicdenv = Dictionary(uniqueKeysWithValues: denv)
            return try invoke(
               exprs: exps, clauses: reductionScheme.patterns, context: [:])
         } else {
            return try .init(expr: .appl(ref, exps.map(recurse)), location: expr.location)
         }
      case let .const(sym):
         let smth = env[sym]
         guard let smth = smth else {
            let err = buildErrorHeader(
               parentLoc: expression.location,
               misbehavedChildLoc: expr.location, text: text, needHeader: true) +
            "\(String(sym.name)) does not reffer to any known definition in scope"
            throw EvaluationProblem(msg: err, code: .lookupFailure)
         }
         if let def = smth as? Definition {
            let evaled = try rewrite(expression: def.value, env: env, text: text)
            return evaled
         }
         return expr
         
      default: return expr
      }
   }
   func invoke(
      exprs: [Expression],
      clauses: [PatternClause],
      context: Dictionary<Symbol, Expression?>)
   throws -> Expression {
      //should reduce appls here?
      let matExps = try exprs.map({ exp -> Expression in
         switch exp.expr {
         case let .const(ref):
            let p = env[ref]
            guard let p = p else {
               let err = buildErrorHeader(
                  parentLoc: expression.location,
                  misbehavedChildLoc: exp.location, text: text, needHeader: true) +
               "Reference \(String(ref.name)) does not reffer to any definition in scope"
               throw EvaluationProblem(msg: err, code: .lookupFailure)
            }
            if let pt = p as? Definition {
               // fix me : use caching
               let nfe = try rewrite(expression: pt.value, env: env, text: text)
               return nfe
            } else { return exp }
         default: return exp
         }
      })
      for clause in clauses {
         let mask = try link(exps: matExps, patt: clause, context: context)
         guard let (exp, ctx) = mask else { continue }
         let final = try bind(exp: exp, context: ctx)
         return try recurse(final)
      }
      let err = exprs.map(renderAsText).joined(separator: "\n")
      let msg = "Error.\n" + err + "\n" + "This input does not match any clause on ?"
      let ermsg = EvaluationProblem(msg: msg, code: .matchFailure)
      throw ermsg
   }
   func deconstruct(
      patt: PatternExpression,
      expr: Expression,
      context: inout Dictionary<Symbol, Expression?>) throws
   -> Bool {
      var result = true
      func recurse(pat: PatternExpression, exp: Expression) throws {
         switch (exp.expr, pat.repr) {
         case (_, .wildcard): ()
         case let (_, .variable(v)):
            //bind var
            let val = context[v]
            if let val2 = val {
               if let val3 = val2, testExactEquality(exp1: val3, exp2: exp) == false {
                  let found = renderAsText(exp)
                  let expected = renderAsText(val3)
                  let err = buildErrorHeader(
                     parentLoc: expression.location,
                     misbehavedChildLoc: exp.location, text: text, needHeader: true)
                  + "Attempt to bind variable '\(String(v.name)) of type '\(found)' to '\(expected)'"
                  let prob = EvaluationProblem(msg: err, code: .unreducedExpression)
                  throw prob
               }
            } else { context[v] = exp }
         case let (.appl(ref, exps), .compound(refr, subps)):
            if ref.filter({ $0 != "\n" && $0 != " " }) ==
                  refr.filter({ $0 != "\n" && $0 != " " }) {
               for (e, sp) in zip(exps, subps) {
                  try recurse(pat: sp, exp: e)
                  //return early
               }
            }
            else { result = false }
         case let (_, .tagged(s, lpat)):
            // bind var
            let val = context[s]
            if let val2 = val {
               if let val3 = val2, testExactEquality(exp1: val3, exp2: exp) == false {
                  let found = renderAsText(exp)
                  let expected = renderAsText(val3)
                  let err = buildErrorHeader(
                     parentLoc: expression.location,
                     misbehavedChildLoc: exp.location, text: text, needHeader: true)
                  + "Attempt to bind variable '\(String(s.name)) of type '\(found)' to '\(expected)'"
                  let prob = EvaluationProblem(msg: err, code: .unreducedExpression)
                  throw prob
               }
            } else { context[s] = exp }
            try recurse(pat: lpat, exp: exp)
         case let (.const(ref1), .refrerence(ref2)):
            if ref1 != ref2 { result = false }
         case (.variable, _):
            let err = buildErrorHeader(
               parentLoc: expression.location,
               misbehavedChildLoc: exp.location, text: text, needHeader: true) +
            "free variable in expression"
            throw EvaluationProblem(msg: err, code: .unreducedExpression)
         default: result = false
         }
      }
      try recurse(pat: patt, exp: expr)
      return result
   }
   func bind(exp: Expression, context: Dictionary<Symbol, Expression?>)
   throws -> Expression {
      switch exp.expr {
      case let .appl(a, exps):
         return try .init(expr: .appl(a, exps.map({
            try bind(exp: $0, context: context)
         })), location: exp.location)
      case .const: return exp
      case let .variable(vari):
         let envexpr = context[vari]!
         guard let exprr = envexpr else {
            let err = buildErrorHeader(
               parentLoc: exp.location,
               misbehavedChildLoc: vari.location, text: text, needHeader: true) +
            "Variable `\(String(vari.name)) has not been bind to a value yet"
            throw EvaluationProblem(msg: err, code: .bindFail)
         }
         return exprr
      }
   }
   func link(
      exps: [Expression],
      patt: PatternClause,
      context: Dictionary<Symbol, Expression?>)
   throws -> (Expression, Dictionary<Symbol, Expression?>)? {
      var localcontext = context
      for (pat, exp) in zip(patt.stencil, exps) {
         if try !deconstruct(patt: pat, expr: exp, context: &localcontext) {
            return nil
         }
      }
      switch patt.rhs {
      case let .monopat(end): return (end, localcontext)
      case let .chainpat(exprs, subclauses):
         let evaled = try exprs.map({ try bind(exp: $0, context: localcontext) })
         return (
            try invoke(exprs: evaled, clauses: subclauses, context: localcontext),
            localcontext)
      }
   }
   return try recurse(expression)
}

public func renderAsText(_ expr: Expression) -> String {
   switch expr.expr {
   case let .appl(ref, exps):
      let args = exps.map({ "(" + renderAsText($0) + ")" })
      var ptr = 0, result = "", ptr2 = 0
      while ptr != ref.endIndex {
         if ref[ptr] == "#" {
            result.append(contentsOf: args[ptr2])
            ptr2 += 1
            ptr += 1
            continue
         }
         result.append(ref[ptr])
         ptr += 1
      }
      return result
   case let .const(ref): return String(ref.name)
   case let .variable(varn): return "`" + String(varn.name)
   }
}

struct Interpreter {
   var enviroment: Env, text: [Character]
   func eval(definitionName: String) throws -> String {
      let key = Symbol(
         name: definitionName.trimmingCharacters(in: .init(charactersIn: "'")).chars,
         location: .fake)
      let def = enviroment[key]
      guard let defn = def else {
         return "Environment does not contain definition \(definitionName)"
      }
      guard let defin = defn as? Definition else {
         return "No definition named \(definitionName) is present in environment"
      }
      let result = try rewrite(expression: defin.value, env: enviroment, text: text)
      return renderAsText(result)
   }
}
