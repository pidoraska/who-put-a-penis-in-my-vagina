//
//  sema.swift
//  sigil
//
//  Created on 16/11/21.
//


public func buildErrorHeader(
   parentLoc: SourceLocation, misbehavedChildLoc: SourceLocation, text: [Character],
   needHeader: Bool)
-> String {
   let str: String
   if needHeader {
      let biggestLine: Int = {
         var ptr = parentLoc.span.lowerBound, pos = ptr, res = 0
         while ptr != parentLoc.span.upperBound {
            if text[ptr] == "\n" {
               if ptr - pos > res { res = ptr - pos }
               pos = ptr
            }
            ptr += 1
         }
         return res
      }()
      str =
      "Error at line \(misbehavedChildLoc.line).\n" +
      String(repeating: "-", count: biggestLine) + "\n"
   } else { str = "" }
   let start: Int = {
      var ptr = parentLoc.span.lowerBound, last = 0
      while ptr != misbehavedChildLoc.span.lowerBound {
         if text[ptr] == "\n" { last = ptr }
         ptr += 1
      }
      if last == 0 {
         return misbehavedChildLoc.span.lowerBound - parentLoc.span.lowerBound
      }
      return last
   }()
   let isMultiline: Bool = {
      var ptr = parentLoc.span.lowerBound
      while ptr != misbehavedChildLoc.span.upperBound {
         if text[ptr] == "\n" { return true }
         ptr += 1
      }
      return false
   }()
   if !isMultiline {
      var ptr = misbehavedChildLoc.span.upperBound
      while ptr != parentLoc.span.upperBound {
         if text[ptr] == "\n" { break }
         ptr += 1
      }
      let padding = String(repeating: " ", count: start)
      let poi = String(
         repeating: "^",
         count: misbehavedChildLoc.span.upperBound - misbehavedChildLoc.span.lowerBound)
      let term: String = String(text[parentLoc.span.upperBound - 1] == "\n" ? "" : "\n")
      return String(
         str + String(text[parentLoc.span.lowerBound ..< ptr]) + "\n" + padding + poi +
         text[ptr ..< parentLoc.span.upperBound]) + term
   }
   
   enum State { case delim, harv }
   var ptr: Int = start, la: Int = 0, res: String = "", delim: Int = 0,
       state: State = State.delim , hack = true
   while true {
      defer { ptr += 1 }
      if ptr == misbehavedChildLoc.span.upperBound {
         if state == .harv {
            let pad: String = String(repeating: " ", count: delim)
            let poi: String = String(repeating: "^", count: ptr - la)
            let st: String = String(
               pad + String(text[la ..< ptr]) + String("\n") + pad + poi + "\n")
            res.append(contentsOf: st)
         }
         break
      }
      switch text[ptr] {
      case "\n":
         if state == .harv {
            let pad: String = String(repeating: " ", count: delim)
            let poi: String = String(repeating: "^", count: ptr - la)
            let st: String = String(
               String(text[la ..< ptr]) + String("\n") + pad + poi + "\n")
            res.append(contentsOf: String((hack ? String("") : pad) + st))
            hack = false
         }
         state = .delim
         delim = 0
      case " ":
         if state == .delim { delim += 1 }
      default:
         if state == .delim { la = ptr }
         state = .harv
      }
   }
   let term: String = String(text[parentLoc.span.upperBound - 1] == "\n" ? "" : "\n")
   return String(
      (str + String(
         text[parentLoc.span.lowerBound ..< misbehavedChildLoc.span.lowerBound]) +
       res + String(
         text[misbehavedChildLoc.span.upperBound ..< parentLoc.span.upperBound]))) +
      term
}

public enum SemanticProblem: Error {
   case msg(String)
}

struct CyclicNumber {
   var n: Int
   let mod: Int
   init(modulo: Int) { mod = modulo; n = 0 }
   static func + (l: CyclicNumber, r: Int) -> CyclicNumber {
      var y = CyclicNumber(modulo: l.mod)
      y.n = r + l.n == l.mod ? l.n / l.mod : r + l.n
      return y
   }
   static func += (l: inout CyclicNumber, r: Int) { l = l + r }
}
public func sanitiseUniverseDecls(_ ud: [UniPack], text: [Character])
-> Either<[Problem], [(SourceLocation, Symbol)]> {
   // fix me : i am horrible
   var result: [(SourceLocation, Symbol)] = [], problems: [Problem] = []
   if ud.count == 1 && ud[0].unis.count == 1 {
      result = [(ud[0].location, ud[0].unis[0])]
   } else {
      let unis: [(SourceLocation, Symbol)] =
      ud.flatMap({ up in up.unis.map({(up.location, $0)})})
      var cpa = 0, cpu = 1
      while true {
         if cpu == unis.endIndex { result.append(unis[cpa]); cpa += 1; cpu = cpa + 1 }
         if cpa == unis.endIndex - 1 { result.append(unis[cpa]); break }
         if unis[cpa].1.name == unis[cpu].1.name {
            let err = buildErrorHeader(
               parentLoc: unis[cpa].0,
               misbehavedChildLoc: unis[cpa].1.location,
               text: text, needHeader: false)
            let conterr = buildErrorHeader(
               parentLoc: unis[cpu].0,
               misbehavedChildLoc: unis[cpu].1.location, text: text,
               needHeader: false)
            let str = "Repeated declaration of universe symbol"
            let sep = String(repeating: "-", count: 50) + "\n"
            problems.append(Problem(
               msg: "Error.\n" + sep + err + conterr + sep + str + "\n",
               code: .dupReference))
         }
         cpu += 1
      }
   }
   if problems.isEmpty { return .right(result) }
   return .left(problems)
}
public func sanitiseRules(_ rs: [RulePack], text: [Character])
throws -> [(SourceLocation, (Symbol, Symbol))] {
   let rules: [(SourceLocation, (Symbol,Symbol))] =
      rs.flatMap({ rp in rp.rules.map({(rp.location,$0)})})
   enum State { case start, cycle }
   var chainHeadIndex = 0, chainTailIndex = CyclicNumber(modulo: rules.endIndex),
       chain: [Int] = [0], state = State.start
   while true {
      if chain.last! == chainTailIndex.n && state == .cycle {
         chainHeadIndex += 1; state = .start; chainTailIndex.n = chainHeadIndex;
         chain = [chainHeadIndex]
      }
      if chainHeadIndex == rules.endIndex { break }
      if rules[chain.first!].1.0.name == rules[chain.last!].1.1.name {
         //(x,_) == (_,x)
         let msg: String = chain.map({ ix in
            "(\(rules[ix].1.0.description) : \(rules[ix].1.1.description))"
         }).joined(separator: " -> ") +
         " => \(rules[chain.first!].1.0) : \(rules[chain.last!].1.1)"
         let pre = chain.map({ ix in
            buildErrorHeader(
               parentLoc: rules[ix].0,
               misbehavedChildLoc: SourceLocation(
                  span: rules[ix].1.0.location.span.lowerBound ..<
                  rules[ix].1.1.location.span.upperBound,
                  line: rules[ix].0.line),
               text: text, needHeader: false)
         }).joined()
         let desc: String  = "The rule\(chain.count > 1 ? "s" : "") " +
         "above form a cyclic dependency which is forbidden"
         let sep = String(repeating: "-", count: 50) + "\n"
         let header = "Error.\n" + sep
         throw Problem(
            msg: header + pre + msg + "\n" + sep + desc,
            code: .circularityInRules)
      }
      if rules[chain.last!].1.1.name == rules[chainTailIndex.n].1.0.name {
         //(_,x) == (x,_)
         chain.append(chainTailIndex.n);
      }
      chainTailIndex += 1
      state = .cycle
   }
   return rules
}
public struct Universum {
   public struct Level {
      let name: Symbol,
          lowerUniverses: Set<Symbol>,
          higherUniverses: Set<Symbol>
   }
   let objects: Set<Level>
}
extension Universum.Level: Equatable, Hashable {
   public func hash(into hasher: inout Hasher) { hasher.combine(name) }
}

public func buildUniversum(
   universeDecls: [UniPack], rulesDecls: [RulePack], text: [Character])
throws -> Either<[Problem], Universum> {
   let unis: [(SourceLocation, Symbol)]
   switch sanitiseUniverseDecls(universeDecls, text: text) {
   case let .left(err) : return .left(err)
   case let .right(val) : unis = val
   }
   let rules = try sanitiseRules(rulesDecls, text: text)
   let res = Universum(objects: Set(unis.map({ (_, us) in
      let supus = Set(rules.compactMap({ us.name == $0.1.0.name ? $0.1.1 : nil }))
      let subus = Set(rules.compactMap({ us.name == $0.1.1.name ? $0.1.0 : nil }))
      return Universum.Level(
         name: us, lowerUniverses: subus, higherUniverses: supus)
   })))
   return .right(res)
}

public struct Environment {
   var data: Set<Data>, universe: Universum, mappings: Set<Mapping>,
       definitions: Set<Definition>
}
public enum Either<L, R> { case left(L), right(R) }
public enum SemanticError {
   case exprHasNonBindableVars,
        dataHasDupCtors,
        circularityInRules,
        invalidReference,
        duplicatesInArgBlock,
        invalidFunlikeRef,
        dupVarsInMatchPattern,
        dupReference,
        invalidPattern,
        attemptToMessWithPrimitive
}
public struct Problem { public let msg: String, code: SemanticError }
extension Problem : Error {}

func findDuplicateCtorNames(in dataDecl: Sigil.Data) -> (Data, [Data.Constructor])? {
   var visited: [Data.Constructor] = [], duped: [Data.Constructor] = []
   for ctor in dataDecl.ctors {
      var isDup = false
      for known in visited {
         if ctor.name == known.name { duped.append(ctor);duped.append(known);isDup = true }
      }
      if !isDup { visited.append(ctor) }
   }
   if duped.isEmpty { return nil }
   return (dataDecl, duped)
}

public typealias Env = Dictionary<Symbol, Any>

extension Dictionary where Key == Symbol, Value == Any {
   var data: Set<Sigil.Data> {
      Set(values.compactMap({$0 is Data ? ($0 as! Data) : nil}))
   }
   var maps: Set<Sigil.Mapping> {
      Set(values.compactMap({$0 is Mapping ? ($0 as! Mapping) : nil}))
   }
   var univ: Set<Sigil.Universum.Level> {
      Set(values.compactMap({$0 is Universum.Level ? ($0 as! Universum.Level) : nil}))
   }
   var dataCtors: Set<Sigil.Data.Constructor> {
      Set(data.map({ $0.ctors }).joined())
   }
   var defns: Set<Definition> {
      Set(values.compactMap({ $0 is Definition ? ($0 as! Definition) : nil }))
   }
}

extension Expression {
   public var isRedex: Bool {
      var flag = true
      func rec(_ expr: Expression) {
         switch expr.expr {
         case .appl(_, let exprs): for expr in exprs { rec(expr) }
         case .const: ()
         case .variable: flag = false
         }
      }
      rec(self)
      return flag
   }
}

extension SourceLocation {
   static public var fake: Self { .init(span: 0..<0, line: 0) }
}

func sanitiseExpression(
   _ expr: Expression, env: Env, text: [Character], parrent: Locateable & ExpressionHolder)
-> [Problem] {
   var problems: [Problem] = []
   func recurse(_ exp: Expression) {
      switch exp.expr {
      case let .appl(ref, exps):
         let val = env[Symbol(name: ref, location: .fake)]
         if val != nil {
            if !(val is Mapping) && !(val is Data) && !(val is Data.Constructor) {
               let err = buildErrorHeader(
                  parentLoc: expr.location,
                  misbehavedChildLoc: exp.location, text: text, needHeader: true) +
               "This expression does not reffer to invokable object"
               problems.append(.init(msg: err, code: .invalidReference))
            }
         } else {
            let err = buildErrorHeader(
               parentLoc: parrent.location,
               misbehavedChildLoc: exp.location, text: text, needHeader: true) +
            "Pattern '\(ref)' does not reffer to any known data constructor in scope"
            problems.append(.init(msg: err, code: .invalidReference))
            for expi in exps { recurse(expi) }
            return
         }
         for expi in exps { recurse(expi) }
      case let .const(s):
         if !parrent.localEnv.contains(where: { $0.0 == s }) {
            if !env.keys.contains(s) {
               let err = buildErrorHeader(
                  parentLoc: parrent.location,
                  misbehavedChildLoc: exp.location, text: text, needHeader: true) +
               "Constant '\(String(s.name))' does not reffer to any known object in scope"
               problems.append(.init(msg: err, code: .invalidReference))
            }
         }
      case .variable(_):()
         
      }
   }
   recurse(expr)
   return problems
}

public func checkArgsForDuplicates(
   _ args: [(Symbol, Expression)], parent: Locateable, text: [Character])
-> Problem? {
   var found : Set<Symbol> = [], dups : Set<Symbol> = []
   for arg in args {
      if found.contains(arg.0) { dups.insert(arg.0) }
      else { found.insert(arg.0) }
   }
   if dups.isEmpty { return nil }
   let msgs = dups.map({
      buildErrorHeader(
         parentLoc: parent.location,
         misbehavedChildLoc: $0.location,
         text: text, needHeader: false)
   })
   return .init(
      msg: "Error.\n" +
      msgs.joined(separator: "\n") + "Argument block contains duplicate names",
      code: .duplicatesInArgBlock)
}
public func sanitisePatterns(_ mapping: Mapping, env: Env, text: [Character]) -> [Problem] {
   var problems: [Problem] = []
   for pat in mapping.patterns {
      var found: Set<Symbol> = [], dups: Set<Symbol> = []
      func recurseOnClause (_ pat: PatternClause, argsCount: Int) {
         for subcomp in pat.stencil { recurseOnPatExpr(subcomp) }
         switch pat.rhs {
         case let .monopat(expr):
            if pat.stencil.count != argsCount {
               let err = buildErrorHeader(
                  parentLoc: mapping.location,
                  misbehavedChildLoc: pat.location, text: text, needHeader: true) +
               "Pattern doesnt not properly deconstruct input.\n" +
               "There \(argsCount > 1 ? "are" : "is") \(argsCount)" +
               " stencil\(argsCount > 1 ? "s" : "") expected, but \(pat.stencil.count) found"
               problems.append(.init(msg: err, code: .invalidPattern))
            }
            //ensure that _output_ expressions in clauses dont have nonbindable vars
            let vars = expr.mentionedVariables
            if !vars.isSubset(of: found) {
               let err = buildErrorHeader(
                  parentLoc: mapping.location,
                  misbehavedChildLoc: expr.location, text: text, needHeader: true) +
               "Expression contains variables that wont be bind: " +
               vars.subtracting(found).map({ "`" + String($0.name) }).joined(separator: " ")
               problems.append(.init(msg: err, code: .exprHasNonBindableVars))
            }
            let probs = sanitiseExpression(expr, env: env, text: text, parrent: mapping)
            problems.append(contentsOf: probs)
         case let .chainpat(exps, pclas):
            for exp in exps {
               // | ... => ? ... =>
               //            ^^^ these
               let errs = sanitiseExpression(exp, env: env, text: text, parrent: mapping)
               problems.append(contentsOf: errs)
            }
            for pcla in pclas { recurseOnClause(pcla, argsCount: exps.count) }
         }
      }
      func recurseOnPatExpr(_ expr: PatternExpression) {
         switch expr.repr {
         case .wildcard: ()
         case let .variable(sym):
            if found.contains(sym) { dups.insert(sym) }
            else { found.insert(sym) }
         case let .compound(ref, pexps):
            let finding = env.dataCtors.first(where: { $0.name.name == ref })
            if finding == nil {
               let err = buildErrorHeader(
                  parentLoc: mapping.location,
                  misbehavedChildLoc: expr.location,
                  text: text, needHeader: true) +
               "This pattern does not reffer to known constructor"
               problems.append(.init(msg: err, code: .invalidReference))
            }
            for pexp in pexps { recurseOnPatExpr(pexp) }
         case let .tagged(sym, pexp):
            if found.contains(sym) { dups.insert(sym) }
            else { found.insert(sym) }
            recurseOnPatExpr(pexp)
         case let .refrerence(ref):
            let local = mapping.localEnv.first(where: {$0.0 == ref})
            if local == nil {
               let global = env.dataCtors.first(where: { $0.name.name == ref.name })
               if global == nil {
                  let err = buildErrorHeader(
                     parentLoc: mapping.location,
                     misbehavedChildLoc: expr.location,
                     text: text, needHeader: true) +
                  "Element of the pattern does not reffer to any known object"
                  problems.append(.init(msg: err, code: .invalidReference))
               }
            }
         }
      }
      recurseOnClause(pat, argsCount: mapping.explicitArgs.args.count)
      if !dups.isEmpty {
         let err = buildErrorHeader(
            parentLoc: mapping.location,
            misbehavedChildLoc: pat.location, text: text, needHeader: true)
         let badvars: String = dups.map({"`" + String.init($0.name)}).joined(separator: " ")
         let msg = "Pattern contains duplicate variables: "
         problems.append(.init(msg: err + msg + badvars, code: .dupVarsInMatchPattern))
      }
   }
   return problems
}
public func sanitiseMappingDecls(env: Env, text: [Character]) -> [Problem] {
   var problems: [Problem] = []
   //check if there are no identical variables in pattern expr &
   for decl in env.maps {
      if decl.reference.inpArgCount != decl.explicitArgs.args.count {
         let err = buildErrorHeader(
            parentLoc: decl.location,
            misbehavedChildLoc: decl.reference.location,
            text: text, needHeader: true) +
         "Name of the mapping does not contain amount of # that is equal to the number of arguments"
         problems.append(.init(msg: err, code: .invalidFunlikeRef))
      }
      if let err = checkArgsForDuplicates(decl.explicitArgs.args, parent: decl, text: text) {
         problems.append(err)
      }
      if let imps = decl.implicitArgs,
         let err = checkArgsForDuplicates(imps.args, parent: decl, text: text)
      {
         problems.append(err)
      }
      problems.append(contentsOf: sanitisePatterns(decl, env: env, text: text))
      
      problems.append(
         contentsOf: sanitiseExpression(
            decl.outputDomain, env: env, text: text, parrent: decl))
      if let imps = decl.implicitArgs?.args {
         for arg in imps {
            let trouble = sanitiseExpression(arg.tvar, env: env, text: text, parrent: decl)
            problems.append(contentsOf: trouble)
         }
      }
      for arg in decl.explicitArgs.args {
         let trouble = sanitiseExpression(arg.tvar, env: env, text: text, parrent: decl)
         problems.append(contentsOf: trouble)
      }
   }
   //check type shapes


   return problems
}

func sanitiseDataDecls(env: Env, text: [Character]) -> [Problem] {
   var problems: [Problem] = []
   
   for decl in env.data {
      if let imps = decl.impArgs?.args {
         for arg in imps {
            let trouble = sanitiseExpression(arg.tvar, env: env, text: text, parrent: decl)
            problems.append(contentsOf: trouble)
         }
      }
   }
   for decl in env.data {
      if let imps = decl.expArgs?.args {
         for arg in imps {
            let trouble = sanitiseExpression(arg.tvar, env: env, text: text, parrent: decl)
            problems.append(contentsOf: trouble)
         }
      }
   }
   
   //name matches exp arg number
   for decl in env.data {
      if let exps = decl.expArgs,
         decl.name.inpArgCount != exps.args.count {
         let err = buildErrorHeader(
            parentLoc: decl.location,
            misbehavedChildLoc: decl.name.location, text: text, needHeader: true) +
         "Name does not have number of #s equal to number of explicit arguments"
         problems.append(.init(msg: err, code: .invalidFunlikeRef))
      }
   }
   
   //no dup ctor check
   for decl in env.data {
      let maybeDups = findDuplicateCtorNames(in: decl)
      if let dups = maybeDups {
         let err = dups.1.map({ dcon in
            buildErrorHeader(
               parentLoc: decl.location,
               misbehavedChildLoc: dcon.location,
               text: text, needHeader: true)
         }).joined()
         let ver = Problem(
            msg: err + "\(decl.name) has several identically named constructors",
            code: .dupReference)
         problems.append(ver)
      }
   }
   //ctor arity check
   for decl in env.data {
      for ctor in decl.ctors {
         // ctor names has correct number of #s
         if ctor.name.inpArgCount != ctor.args.count {
            let err = buildErrorHeader(
               parentLoc: decl.location,
               misbehavedChildLoc: ctor.name.location,
               text: text, needHeader: true) +
            "Name of the mapping does not contain amount of # that is equal to the number of arguments"
            problems.append(.init(msg: err, code: .invalidFunlikeRef))
         }
      }
      if let exps = decl.expArgs,
         let err = checkArgsForDuplicates(exps.args, parent: decl, text: text) {
         problems.append(err)
      }
      if let exps = decl.impArgs,
         let err = checkArgsForDuplicates(exps.args, parent: decl, text: text) {
         problems.append(err)
      }
   }
   //ensure ulex is correct
   for decl in env.data {
      let err = sanitiseExpression(decl.ulevexpr, env: env, text: text, parrent: decl)
      problems.append(contentsOf: err)
   }
   return problems
}

public func sanitiseDefns(env: Env, text: [Character]) -> [Problem] {
   var problems: [Problem] = []
   
   for defn in env.defns {
      if let imps = defn.implicitArgs?.args {
         if let prob = checkArgsForDuplicates(imps, parent: defn, text: text) {
            problems.append(prob)
         }
      }
      problems.append(
         contentsOf: sanitiseExpression(defn.type, env: env, text: text, parrent: defn))
      problems.append(
         contentsOf: sanitiseExpression(defn.value, env: env, text: text, parrent: defn))
   }
   return problems
}

public func buildEnvironment(from rawEnv: [RawItem], text: [Character])
-> Either<[Problem],Env> {
   
   var problems: [Problem] = []
   let rules: [RulePack] = rawEnv.compactMap({
      if case .rules(let dd) = $0 { return dd }
      return nil
   })
   let univs: [UniPack] = rawEnv.compactMap({
      if case .universe(let dd) = $0 { return dd }
      return nil
   })
   var universum : Universum? = nil
   do {
      switch try buildUniversum(universeDecls: univs, rulesDecls: rules, text: text) {
      case let .left(errs): problems.append(contentsOf: errs)
      case let .right(u): universum = u
      }
   } catch let err as Problem {
      problems.append(err)
   } catch { fatalError("buildUniversum thrown something unexpected") }
   
   guard let universum = universum else { return .left(problems) }
   
   var symbolTable : Dictionary<Symbol, Any> = [:]
   
   for ul in universum.objects { symbolTable[ul.name] = ul }
   
   let data: [Data] = rawEnv.compactMap({
      if case .datadecl(let dd) = $0 { return dd }
      return nil
   })
   let maps: [Mapping] = rawEnv.compactMap({
      if case .mapping(let dd) = $0 { return dd }
      return nil
   })
   let definitions: [Definition] = rawEnv.compactMap({
      if case let .defn(defn) = $0 { return defn }
      return nil
   })
   for defn in definitions {
      if symbolTable.keys.contains(defn.name) {
         let msg = buildErrorHeader(
            parentLoc: defn.location,
            misbehavedChildLoc: defn.name.location, text: text, needHeader: true) +
         "Duplicated definition"
         problems.append(.init(msg: msg, code: .dupReference))
      }
      else { symbolTable[defn.name] = defn }
   }
   for defn in data {
      if symbolTable.keys.contains(defn.name) {
         let msg = buildErrorHeader(
            parentLoc: defn.location,
            misbehavedChildLoc: defn.name.location, text: text, needHeader: true) +
         "Duplicated definition"
         problems.append(.init(msg: msg, code: .dupReference))
      }
      else { symbolTable[defn.name] = defn }
   }
   for defn in maps {
      if symbolTable.keys.contains(defn.reference) {
         let msg = buildErrorHeader(
            parentLoc: defn.location,
            misbehavedChildLoc: defn.reference.location, text: text, needHeader: true) +
         "Duplicated definition"
         problems.append(.init(msg: msg, code: .dupReference))
      }
      else { symbolTable[defn.reference] = defn }
   }
   
   if let th = symbolTable.keys.first(where: { $0.name == "max##".chars }) {
      let err = buildErrorHeader(
         parentLoc: th.location,
         misbehavedChildLoc: th.location, text: text, needHeader: true) +
      "'max # #' is a builtin primitive definition and cannot be a standalone definition"
      problems.append(.init(msg: err, code: .attemptToMessWithPrimitive))
      return .left(problems)
   }
   
   guard problems.isEmpty else { return .left(problems) }
   let dataSanityVerdict = sanitiseDataDecls(env: symbolTable, text: text)
   problems.append(contentsOf: dataSanityVerdict)
   guard problems.isEmpty else { return .left(problems) }
   
   for datum in data {
      for ctor in datum.ctors {
         if symbolTable.keys.contains(ctor.name) {
            let err = buildErrorHeader(
               parentLoc: datum.location,
               misbehavedChildLoc: ctor.location, text: text, needHeader: true) +
            "Name of constructor coincides with another declaration within scope"
            problems.append(.init(msg: err, code: .dupReference))
         } else {
            symbolTable[ctor.name] = ctor
         }
      }
   }
   problems.append(contentsOf: sanitiseDefns(env: symbolTable, text: text))
   guard problems.isEmpty else { return .left(problems) }
   
   let errs = sanitiseMappingDecls(env: symbolTable, text: text)
   problems.append(contentsOf: errs)
   
   guard problems.isEmpty else { return .left(problems) }
   
   return .right(symbolTable)
}

