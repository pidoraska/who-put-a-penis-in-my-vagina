// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "casper-pragma",
    products: [
      .executable(name: "Knot", targets: ["KnotExe"]),
      .library(name: "SigilGuts", targets: ["Sigil"])
    ],
    targets: [
      .target(name: "Sigil", path: "Sources/Sigil"),
      .executableTarget(
         name: "KnotExe", dependencies: ["Sigil"], path: "Sources/Knot"),
      .testTarget(
         name: "ParserChecks",
         dependencies: ["Sigil"],
         path: "Tests")
    ]
)
